#!/usr/bin/env bash
docker network create container-educaio-network
docker-compose -f educaio-deploy/dev/docker-compose.yml pull
docker-compose -f educaio-deploy/dev/docker-compose.yml down
docker-compose -f educaio-deploy/dev/docker-compose.yml up -d
docker restart gateway-educaio-container
