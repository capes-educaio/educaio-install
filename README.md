# Capes Educa-io - Instalação via Docker

Arquivos necessários para fazer a instalação, via docker, do software Capes Educa-IO.

## Requisitos para uma instalação local (localhost)

1. Distribuição Linux atualizada
2. [Docker](https://www.docker.com/)
3. [Docker Compose](https://docs.docker.com/compose/install/)
4. Porta 80 disponível

## Instruções

1. Clone o repositório:
   > `$ git clone https://gitlab.com/capes-educaio/educaio-deploy.git`

2. Entre no diretório
   > `$ cd educaio-deploy`

3. Escolha se deseja instalar a versão em desenvolvimento (dev) ou a versão mais atual em produção (master). Então
   > `$ ./educaio-deploy/dev/update.sh`  (desenvolvimento)

   > `$ ./educaio-deploy/mater/update.sh`  (produção)

4. Adicione as linhas seguintes ao arquivo /etc/hosts
   ```
   127.0.0.1  api.localhost
   127.0.0.1  api.dev.localhost
   ```

5. Abra o navegador e acesse `http://localhost` ou `http://dev.localhost`, dependendo da instalação escolhida.

# Instalação manual dos microserviços (apenas para desenvolvimento)

As instruções de download e de instalação de cada microserviço são encontradas nos respectivos projetos, no grupo [capes-educaio do gitlab](https://gitlab.com/capes-educaio).